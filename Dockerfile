FROM openjdk:8-jre-alpine
EXPOSE 8080
ADD /build/libs/DockerLab-1.0.1.jar DockerLab.jar
ENTRYPOINT ["java","-jar","DockerLab.jar"]