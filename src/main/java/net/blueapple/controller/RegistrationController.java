package net.blueapple.controller;

import net.blueapple.user.User;
import net.blueapple.user.UserDto;
import net.blueapple.user.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.validation.Valid;
import java.util.UUID;

/**
 * Created by Lithium on 6/22/2017.
 */
@Controller
public class RegistrationController {
    @Autowired
    private UserService userService;

    @RequestMapping(path = "/register",method = RequestMethod.GET)
    public String showRegistrationPage(Model model) {
        model.addAttribute("userDto",new UserDto());
        String uuid= UUID.randomUUID().toString().substring(0,5);
        userService.register(new UserDto("ahri-"+uuid,"neri","neri-"+uuid+"@gmail.com"));
        return "registration";
    }
    @RequestMapping(path = "/register",method = RequestMethod.POST)
    public String processRegistration(@Valid @ModelAttribute(name = "userDto") UserDto userDto, BindingResult result) {
        if(result.hasErrors()) return "registration";
        User user=userService.register(userDto);
        System.out.println("Registration "+user.getUsername()+" success");
        return "redirect:/";
    }
}
