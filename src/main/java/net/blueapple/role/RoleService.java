package net.blueapple.role;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by Lithium on 6/22/2017.
 */
@Service
public class RoleService {
    @Autowired
    private RoleRepository roleRepository;

    public RoleService() {
    }

    public Role findByName(String name) {
        return roleRepository.findByName(name);
    }
}
