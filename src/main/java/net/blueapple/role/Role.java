
package net.blueapple.role;

import net.blueapple.privilege.Privilege;
import net.blueapple.user.User;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by Lithium on 6/22/2017.
 */
@Entity
public class Role {
    @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "role_id")
    private long id;
    private String name;

    @ManyToMany(fetch = FetchType.LAZY,mappedBy = "roles")
    private Set<User> users;
    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name = "role_privilege",
        joinColumns = @JoinColumn(name = "role_id",referencedColumnName = "role_id"),
        inverseJoinColumns = @JoinColumn(name = "privilege_id",referencedColumnName = "privilege_id"))
    private Set<Privilege> privileges;

    public Role() {
        this.privileges=new HashSet<>();
    }

    public Role(String name) {
        this.name = name;
        this.privileges=new HashSet<>();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Set<User> getUsers() {
        return users;
    }

    public void setUsers(Set<User> users) {
        this.users = users;
    }

    public Set<Privilege> getPrivileges() {
        return privileges;
    }

    public void setPrivileges(Set<Privilege> privileges) {
        this.privileges = privileges;
    }
}
