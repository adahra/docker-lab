package net.blueapple.privilege;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by Lithium on 6/22/2017.
 */
@Service
public class PrivilegeService {
    @Autowired
    private PrivilegeRepository privilegeRepository;

    public PrivilegeService() {
    }

    public Privilege findByName(String name) {
        return privilegeRepository.findByName(name);
    }
}
