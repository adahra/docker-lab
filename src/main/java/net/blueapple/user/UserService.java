package net.blueapple.user;

import net.blueapple.role.RoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by Lithium on 6/22/2017.
 */
@Service
public class UserService {
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private RoleService roleService;

    public UserService() {
    }

    public User findByUsername(String name) {
        return userRepository.findByUsername(name);
    }

    public User register(UserDto userDto) {
        User user=new User(userDto);
        user.setPassword(new BCryptPasswordEncoder().encode(user.getPassword()));
        user.addRole(roleService.findByName("MEMBER"));
        return userRepository.save(user);
    }

    public List<User> findAll(){
        return userRepository.findAll();
    }
}
