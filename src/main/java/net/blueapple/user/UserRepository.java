package net.blueapple.user;

import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by Lithium on 6/22/2017.
 */
public interface UserRepository extends JpaRepository<User,Long> {

    User findByUsername(String username);
}
